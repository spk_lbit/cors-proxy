const express = require('express');
const request = require('request');
const bodyParser = require('body-parser');
const config = require('./config')

const app = express();
app.use(bodyParser.json());

app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, PATCH, POST, DELETE");
    res.header("Access-Control-Allow-Headers", req.header('access-control-request-headers'));
    if (req.method === 'OPTIONS') {
        res.send();
    } else {
       req.headers.cookie = config.COOKIE_VALUE;
        request({ url: config.TARGET_URL + req.url, method: req.method, json: req.body, headers: req.headers },
            function (error, response, body) {
                if (error) {
                    console.error('error: ' + response.statusCode)
                }
            }).pipe(res);
    }
});
app.set('port', config.PORT || 5005);
app.listen(app.get('port'), function (x) {
    console.log(`Proxy server listening on port http://${config.PORXY_HOSTNAME || 'localhost'}:${app.get('port')}`);
});